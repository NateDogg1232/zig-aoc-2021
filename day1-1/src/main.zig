const std = @import("std");

var integers: [0x1000]usize = undefined;
var int_count: usize = undefined;

pub fn main() anyerror!void {
    try load_and_parse_file();
    var inc: usize = 0;
    while (i <= int_count) : (i += 1) {
        if (integers[i] > integers[i - 1]) {
            inc += 1;
        }
    }
    std.debug.print("{}", .{inc});
}

pub fn load_and_parse_file() anyerror!void {
    // Open the file in the current directory
    var file = try std.fs.cwd().openFile("input.txt", .{});
    // Close the file at the end of this scope
    defer file.close();

    // Create a buffered reader (for better performance)
    var buf_reader = std.io.bufferedReader(file.reader());
    // And create the input stream
    var in_stream = buf_reader.reader();

    // Go line by line and append all of it to
    // a list of integers
    var buffer: [1028]u8 = undefined;
    int_count = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buffer, '\n')) |line| : (int_count += 1) {
        integers[int_count] = try std.fmt.parseInt(usize, line, 10);
    }
    int_count -= 1;
}
