const std = @import("std");

var integers: [0x1000]isize = undefined;
const Direction = enum {
    forward,
    up,
    down,
};
var directions: [0x1000]Direction = undefined;
var int_count: usize = undefined;

pub fn main() anyerror!void {
    try load_and_parse_file();

    var i: usize = 0;
    var horiz: isize = 0;
    var depth: isize = 0;
    var aim: isize = 0;
    while (i <= int_count) : (i += 1) {
        if (directions[i] == Direction.forward) {
            horiz += integers[i];
            depth += aim * integers[i];
        } else if (directions[i] == Direction.up) {
            aim -= integers[i];
        } else if (directions[i] == Direction.down) {
            aim += integers[i];
        } else {
            std.debug.panic("Found unknown direction: {any}", .{directions[i]});
        }
    }
    std.debug.print("depth: {}, horiz: {}, mult: {}\n", .{ depth, horiz, depth * horiz });
}

pub fn load_and_parse_file() anyerror!void {
    // Open the file in the current directory
    var file = try std.fs.cwd().openFile("input.txt", .{});
    // Close the file at the end of this scope
    defer file.close();

    // Create a buffered reader (for better performance)
    var buf_reader = std.io.bufferedReader(file.reader());
    // And create the input stream
    var in_stream = buf_reader.reader();

    // Go line by line and append all of it to
    // a list of integers
    var buffer: [1028]u8 = undefined;
    int_count = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buffer, '\n')) |line| : (int_count += 1) {
        var word_counter: bool = false;
        var split = std.mem.split(line, " ");
        while (split.next()) |word| : (word_counter = word_counter != true) {
            if (word_counter == false) {
                if (std.mem.eql(u8, word, "forward")) {
                    directions[int_count] = Direction.forward;
                } else if (std.mem.eql(u8, word, "up")) {
                    directions[int_count] = Direction.up;
                } else if (std.mem.eql(u8, word, "down")) {
                    directions[int_count] = Direction.down;
                } else {
                    std.debug.panic("Incorrect word: {s}", .{word});
                }
            } else {
                integers[int_count] = try std.fmt.parseInt(isize, word, 10);
            }
        }
    }
    int_count -= 1;
}
